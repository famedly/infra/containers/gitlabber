FROM docker.io/python:alpine
RUN apk add --no-cache git \
 && pip install gitlabber
